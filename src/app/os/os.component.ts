import { Component, OnInit, Output } from '@angular/core';
import { Os } from './os';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { map, mergeMap } from 'rxjs/operators';
import { Observable, from } from 'rxjs';
import { Tcp } from '../tcp/tcp';
import { Produto } from '../produto/produto';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalreforcoComponent } from './modalreforco/modalreforco.component';
import { isString } from 'util';


@Component({
  selector: 'app-os',
  templateUrl: './os.component.html'
})
export class OsComponent implements OnInit {
  bsModalRef: BsModalRef;
  mkey:string;
  keybind:string;
  tempvar:string;

  @Output() os:Os;
  osO:Observable<any[]>;
  osRef: AngularFireList<any>;
  osK: Observable<any[]>;
  prarray: string[];
  prdarray:string[] = [];


  @Output() tcp:Tcp;
  tcpK: Observable<any[]>;
  tcpRef: AngularFireList<any>;
  
  @Output() produto:Produto;
  prdK:Observable<any[]>;
  prdRef: AngularFireList<any>;
  produtos$:any[];


  

  constructor(public db:AngularFireDatabase,private modalService: BsModalService) {
    
    //Instancias
    this.produtos$=[];   
    this.prarray = [];
    this.prdarray = [];
    this.keybind=undefined;
    this.os = new Os();
    this.osRef = db.list('/Os');
    //Concat Data
    this.osK = this.osRef.snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ 
          key: c.payload.key,
          tcp: this.queryTcp(c.payload.val().os.tcp),
          produto: this.queryProd(c.payload.val().os.produto),
          os:c.payload.val() 
        }))
      )
    );
    // this.osK.subscribe(snapshots => {
    //     snapshots.forEach(snapshot => {
    //       console.log(snapshot.key)
          
          
  
    //     });
    //   })
    //TCP conect
    this.tcp = new Tcp();
    this.tcpRef=db.list('/Tcp');
    this.tcpK = this.tcpRef.snapshotChanges().pipe(
      map(changes=>
      changes.map(c =>({
        key: c.payload.key,
        tcp:c.payload.val()
      })))
    )
    //Produto conect
    this.produto = new Produto();
    this.prdRef = db.list('/Produto');
    this.prdK = this.prdRef.snapshotChanges().pipe(
      map(changes=>
      changes.map(c =>({
        key: c.payload.key,
        produto: c.payload.val()
      })))
    )


    this.exibe();
    
   }

  ngOnInit() {
  }

  exibe(){
    // const tcpN:Observable<any[]> = this.queryTcp('-LFSO40RhRRdEBPVsjbf');
    // tcpN.subscribe(snapshots => {
    //   snapshots.forEach(snapshot => {
    //     console.log(snapshot.key)
    //     console.log(snapshot.nome);
        

    //   });
    // })
    //this.saveondb();
    //console.log(this.os);
    
  }
  saveondb(){
    const d:any = this.os.data;
    this.os.data = this.convertedata(d);
    this.os.praga = [];
    this.os.praga = this.prarray;
    this.os.produto = [];
    this.os.produto = this.prdarray;
    this.db.list('/Os').push({ 
      os: this.os
    });
    this.clear();
  }
  remove(item){
    this.osRef.remove(item.key);
    console.log(item.key +' Foi removido com sucesso');
  }

  update(){
    const d:any = this.os.data;
    if (isString(d)=== false){
      this.os.data = this.convertedata(d);
    }
    this.os.praga = [];
    this.os.praga = this.prarray;
    this.os.produto = [];
    this.os.produto = this.prdarray;
    this.osRef.set(this.keybind,{
      os: this.os
    })
    this.clear();
  }

  blind(item){
    this.prarray=[];
    this.prdarray = [];
    this.keybind=item.key;
    this.os=item.os.os;
    this.prarray = item.os.os.praga;
    this.prdarray = item.os.os.produto;
    this.prdmtc();
  }

  clear(){
    this.os = new Os();
    this.keybind=undefined;
    this.prarray=[];
    this.prdarray=[];
  }

  queryTcp(as:string){
    const a= as;
    const tcpN :Observable<any[]>= this.db.list('/Tcp/'+a).valueChanges();
    return tcpN;
  }
  queryProd(as:string){
    const a= as;
    const prdN :Observable<any[]>= this.db.list('/Produto/'+a).valueChanges();
    return prdN;
  }
  

  convertedata(dt:Date):string{
    let d;let m;let a;
    if (dt.getDate() < 10) { d = '0' + dt.getDate() } else { d = dt.getDate()  }
    if (dt.getMonth() < 10) { m = '0' + dt.getMonth() } else { m = dt.getMonth() }
    a= dt.getFullYear();
    return d+'/'+m+'/'+a
  }

  addpr(a:string){
    if( this.prarray != undefined ){
      this.prarray.push(a)
    }else{
      this.prarray = [];
      this.prarray.push(a);
    }
  }
  rmpr(a:string){
    var index = this.prarray.indexOf(a);
    if (index !== -1) this.prarray.splice(index, 1);
  }

  addprd(a:string){
    if( this.prdarray != undefined ){
      this.prdarray.push(a)
    }else{
      this.prdarray = [];
      this.prdarray.push(a);
    }        
    this.prdmtc();
  }

  prdmtc(){
    this.produtos$=[];
    this.getItems(this.prdarray).subscribe(item=>this.produtos$.push(item));
  }

  rmprd(a:string){
    var index = this.prdarray.indexOf(a);
    if (index !== -1) this.prdarray.splice(index, 1);
    this.prdmtc();
  }

  
   queryProdL(as:string){
    return this.db.list(`/Produto/${as}`).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ 
          key: as,
          prod: c.payload.val()
        }))
      )
    );
  }
  
  getItems(ids: string[]): Observable<Produto[]> {
   return from(ids).pipe(
      mergeMap(id => <Observable<any[]> > this.queryProdL(id))
    );
  }

  modalprod(item){
    this.prdarray = item.os.os.produto;
    this.prdmtc();
    const initialState = {
      title: `Produtos usados em OS ${item.os.os.nome}`,
      prd: this.produtos$

    };
    this.clear();
    this.bsModalRef = this.modalService.show(ModalreforcoComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }
}