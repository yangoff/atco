import { Component, OnInit, Output } from '@angular/core';
import { User } from './user';
import { Observable } from 'rxjs';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html'
})
export class UserComponent implements OnInit {
  @Output() user:User;
  userK: Observable<any[]>;
  userRef: AngularFireList<any>;
  keybind:string;
  @Output() ppas:string;

  constructor(public db:AngularFireDatabase) { 
    this.keybind=undefined;
    this.user = new User();
    this.userRef=db.list('/User');
    this.userK = this.userRef.snapshotChanges().pipe(
      map(changes=>
      changes.map(c =>({
        key: c.payload.key,
        user:c.payload.val()
      })))
    )
    
  }

  ngOnInit() {
  

  }

  clear(){
    this.keybind=undefined;
    this.user = new User();
    this.ppas='';
  }

  saveondb(){
    this.db.list('/User').push({ 
      user: this.user
    });
    this.clear();
  }

  update(){
    this.userRef.set(this.keybind,{
      user: this.user
    })
    this.clear();
  }

  blind(item){
    this.keybind=item.key;
    this.user=item.user.user;
  }
  remove(item){
    this.userRef.remove(item.key);
    console.log(item.key +' Foi removido com sucesso');
  }


}
