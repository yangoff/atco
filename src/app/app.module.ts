import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { PreloadAllModules, RouterModule } from '@angular/router';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ModalModule,BsDatepickerModule } from 'ngx-bootstrap';
import { environment } from '../environments/environment';
import { AppComponent } from './app.component';
import { ROUTES } from './app.routs';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ModalreforcoComponent } from './os/modalreforco/modalreforco.component';
import { OsComponent } from './os/os.component';
import { ProdutoComponent } from './produto/produto.component';
import { TcpComponent } from './tcp/tcp.component';
import { UserComponent } from './user/user.component';
import { ReforcoComponent } from './reforco/reforco.component';
import { AuthService } from './services/auth.service';
import { LoginComponent } from './login/login.component';
import { AngularFireAuth } from '../../node_modules/angularfire2/auth';
import { AuthGuard } from './services/auth-guard.service';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TcpComponent,
    HeaderComponent,
    UserComponent,
    OsComponent,
    ProdutoComponent,
    ModalreforcoComponent,
    ReforcoComponent,
    LoginComponent
  ],
  imports: [
    ModalModule.forRoot(),
    BrowserModule,
    RouterModule.forRoot(ROUTES,{preloadingStrategy:PreloadAllModules}),
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    BsDatepickerModule.forRoot()
  ],
  providers: [AngularFireModule, FormsModule,ReactiveFormsModule,OsComponent,AuthService,AngularFireAuth,AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [
    ModalreforcoComponent,
    ]
})
export class AppModule { }
