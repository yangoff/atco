import { Component, OnInit, Output } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { Tcp } from './tcp';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-tcp',
  templateUrl: './tcp.component.html'
})
export class TcpComponent implements OnInit {
  tcpK: Observable<any[]>;
  tcpRef: AngularFireList<any>;
  @Output() tcp:Tcp;
  keybind:string;
  constructor(public db: AngularFireDatabase) {
    this.keybind=undefined;
    this.tcp = new Tcp();
    this.tcpRef=db.list('/Tcp');
    this.tcpK = this.tcpRef.snapshotChanges().pipe(
      map(changes=>
      changes.map(c =>({
        key: c.payload.key,
        tcp:c.payload.val()
      })))
    )
  }

  ngOnInit() {
   
  }
  
  capturadados(){
    console.log("Nome: "+this.tcp.nome+ "| Registro: "+this.tcp.registro+" | Nivel: "+this.tcp.nivel);
  }
  clear(){
    this.tcp = new Tcp();
    this.keybind=undefined;
  }

  

  savedata(){
    this.db.list('/Tcp').push({
      tcp: this.tcp
    })
    this.clear();
  }

  mostrardados(){
    this.tcpK.subscribe(snapshots => {
      snapshots.forEach(snapshot => {
        console.log(snapshot.key)
        console.log(snapshot.tcp.tcp.nome)

      });
    })
  }
  
  remove(item){
    this.tcpRef.remove(item.key);
    console.log(item.key +' Foi removido com sucesso');
  }

  update(){
    this.tcpRef.set(this.keybind,{
      tcp: this.tcp
    })
    this.clear();
  }

  blind(item){
    this.keybind=item.key;
    this.tcp=item.tcp.tcp;
  }

}
