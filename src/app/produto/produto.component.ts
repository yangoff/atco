import { Component, OnInit, Output } from '@angular/core';
import { Produto } from './produto';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html'
})
export class ProdutoComponent implements OnInit {
  
  prdK: Observable<any[]>;
  prdRef: AngularFireList<any>;
  @Output() prod:Produto;
  keybind:string;
  constructor(public db: AngularFireDatabase) {
    this.keybind=undefined;
    this.prod = new Produto();
    this.prdRef=db.list('/Produto');
    this.prdK = this.prdRef.snapshotChanges().pipe(
      map(changes=>
      changes.map(c =>({
        key: c.payload.key,
        prd:c.payload.val()
      })))
    )
   }

  ngOnInit() {
  }

  saveondb(){
    this.db.list('/Produto').push({
      produto : this.prod
    })
    this.clear();
  }
  blind(item){
    this.keybind=item.key;
    this.prod=item.prd.produto;
  }
  remove(item){
    this.prdRef.remove(item.key);
    console.log(item.key +' Foi removido com sucesso');
  }
  clear(){
    this.prod = new Produto();
    this.keybind= undefined;
  }
  update(){
    this.prdRef.set(this.keybind,{
      produto: this.prod
    })
    this.clear();
  }

}
