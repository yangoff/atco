import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularFireList, AngularFireDatabase, snapshotChanges } from 'angularfire2/database';
import { Observable, from } from 'rxjs';
import { Os } from '../os/os';
import { Reforco } from './reforco';
import { Tcp } from '../tcp/tcp';
import { map, mergeMap } from 'rxjs/operators';
import { Produto } from '../produto/produto';
import { isString } from 'util';
import { OsComponent } from '../os/os.component';
import { ModalreforcoComponent } from '../os/modalreforco/modalreforco.component';
import { BsModalService, BsModalRef } from '../../../node_modules/ngx-bootstrap';

@Component({
  selector: 'app-reforco',
  templateUrl: './reforco.component.html'
})
export class ReforcoComponent implements OnInit {
  bsModalRef: BsModalRef;

  //Refoco
  @Output() refo: Reforco;
  refK: Observable<any[]>;
  rfRef: AngularFireList<any>;
  keybind: any;
  qs: string;

  //TCP
  @Output() tcp: Tcp;
  tcpK: Observable<any[]>;
  tcpRef: AngularFireList<any>;

  //OS
  @Output() os: Os;
  osK: Observable<any[]>;
  osRef: AngularFireList<any>;
  keybindos: any;

  //Produto
  @Output() produto: Produto;
  prdK: Observable<any[]>;
  prdRef: AngularFireList<any>;
  prdArray: string [];
  produtos$:any[];
  prdREF$:any[];

  layRF:boolean;

  //Contrutor --Conect and load data observables in firebase-- REALTIME/OBSERVABLE
  constructor(private route: ActivatedRoute, public db: AngularFireDatabase, public oc: OsComponent,private modalService: BsModalService) {
    this.produtos$=[];
    this.prdArray = [];
    this.prdREF$ = [];
    this.layRF = false;

    //Params os
    this.os = new Os();
    this.keybindos = this.route.snapshot.params['id']

    //Instances
    this.refo = new Reforco();

    //TCP conect
    this.tcp = new Tcp();
    this.tcpRef = db.list('/Tcp');
    this.tcpK = this.tcpRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.key,
          tcp: c.payload.val()
        })))
    )

    // OS
    this.osRef = db.list(`/Os/${this.keybindos}`);
    //Concat Data
    this.osK = this.osRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.key,
          tcp: this.queryTcp(c.payload.val().tcp),
          os: c.payload.val(),
          prd:c.payload.val().produto
        }))
      )
    );  
    this.osK.subscribe(snp =>{
      snp.forEach(element => {
       this.popula(element.prd);  
     });
    })
       
    
    
  
    //Reforco
    this.rfRef = db.list(`/Os/${this.keybindos}/os/reforco`);
    //Concat Data
    this.refK = this.rfRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.key,
          ref: c.payload.val(),
          tcp: this.queryTcp(c.payload.val().refo.tcp),
          keyos: this.keybindos,
          produto: this.queryProd(c.payload.val().refo.produto)
        }))
      )
    )

    //Produto conect
    this.produto = new Produto();
    this.prdRef = db.list('/Produto');
    this.prdK = this.prdRef.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({
          key: c.payload.key,
          produto: c.payload.val()
        })))
    )
  }

  ngOnInit() {
  }

  // queryOs(as: string) {
  //   const a = as;
  //   const osT: AngularFireList<any> = this.db.list('/Os/' + a);
  //   const osN: Observable<any[]> = osT.snapshotChanges().pipe(
  //     map(changes =>
  //       changes.map(c => ({
  //         key: c.payload.key,
  //         tcp: this.queryTcp(c.payload.val()),
  //         produto: this.queryProd(c.payload.val()),
  //         os: c.payload.val()
  //       }))
  //     )
  //   )
  //   osN.subscribe(snapshots => {
  //       snapshots.forEach(snapshot => {
  //         console.log(snapshot)
  //       });
  //     })

  // }
  queryTcp(as: string) {
    const a = as;
    const tcpN: Observable<any[]> = this.db.list('/Tcp/' + a).valueChanges();
    return tcpN;
  }
  queryProd(as: string) {
    const a = as;
    const prdN: Observable<any[]> = this.db.list('/Produto/' + a).valueChanges();
    return prdN;
  }
  convertedata(dt: Date): string {
    let d; let m; let a;
    if (dt.getDate() < 10) { d = '0' + dt.getDate() } else { d = dt.getDate() }
    if (dt.getMonth() < 10) { m = '0' + dt.getMonth() } else { m = dt.getMonth() }
    a = dt.getFullYear();
    return d + '/' + m + '/' + a
  }
  saveondb() {
    const d: any = this.refo.data;
    this.refo.data = this.convertedata(d);
    this.refo.oskey = this.keybindos;
    this.refo.produto=[];
    this.refo.produto = this.prdArray;
    this.db.list(`/Os/${this.keybindos}/os/reforco`).push({
      refo: this.refo
    });
    this.clear();
    this.layF();
    
  }
  clear(){
    this.refo = new Reforco();
    this.keybind=undefined;
    this.prdArray = [];
  }

  blind(item){
    this.prdArray = [];
    this.keybind=item.key;
    this.refo=item.ref.refo;
    this.prdArray = item.ref.refo.produto;
    this.prdmtc();
  }

  update(){
    const d:any = this.refo.data;
    if (isString(d)=== false){
    this.refo.data = this.convertedata(d);
    }
    this.refo.produto=[];
    this.refo.produto = this.prdArray;
    this.rfRef.set(this.keybind,{
      refo: this.refo
    })
    this.clear();
    this.layF();

  }
  remove(item){
    this.rfRef.remove(item.key);
    console.log(item.key +' Foi removido com sucesso');
  }
  //PRD LAYOUT
  popula(a:string[]){
    if(this.produtos$.length <= 0){
      this.getItems(a).subscribe(item=>this.produtos$.push(item));  
    }
  }
  queryProdL(as:string){
    return this.db.list(`/Produto/${as}`).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ 
          key: as,
          prod: c.payload.val()
        }))
      )
    );
  }
  getItems(ids: string[]): Observable<Produto[]> {
   return from(ids).pipe(
      mergeMap(id => <Observable<any[]> > this.queryProdL(id))
    );
  }
  //PRD REF
  addprdR(a:string){
    this.layRF = true;
    if( this.prdArray != undefined ){
      this.prdArray.push(a)
    }else{
      this.prdArray = [];
      this.prdArray.push(a);
    }        
    this.prdmtc();
  }

  prdmtc(){
    this.prdREF$=[];
    this.getItems(this.prdArray).subscribe(item=>this.prdREF$.push(item));
  }

  rmprdR(a:string){
    var index = this.prdArray.indexOf(a);
    if (index !== -1) this.prdArray.splice(index, 1);
    if (this.prdArray.length > 0) {
      this.prdmtc();
    } else {
      this.layF();
    }
    
  }

  
   queryProdRF(as:string){
    return this.db.list(`/Produto/${as}`).snapshotChanges().pipe(
      map(changes => 
        changes.map(c => ({ 
          key: as,
          prod: c.payload.val()
        }))
      )
    );
  }
  
  getItemsRF(ids: string[]): Observable<Produto[]> {
   return from(ids).pipe(
      mergeMap(id => <Observable<any[]> > this.queryProdL(id))
    );
  }

  layT(){
    if(this.prdArray.length > 0){
      this.layRF = true;
    }
    
  }
  layF(){
    this.layRF = false;
  }

  modalprod(item){
    this.prdArray = item.ref.refo.produto;
    this.prdmtc();
    const initialState = {
      title: `Produtos usados em Reforço`,
      prd: this.prdREF$

    };
    this.clear();
    this.bsModalRef = this.modalService.show(ModalreforcoComponent, {initialState});
    this.bsModalRef.content.closeBtnName = 'Close';
  }


}
