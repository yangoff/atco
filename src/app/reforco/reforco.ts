import { Tcp } from "../tcp/tcp";
import { Produto } from "../produto/produto";


export class Reforco {
    public oskey:string;
    public tcp:Tcp;
    public vendedor:string;
    public data:string;
    public produto:string[];

    constructor(){
        this.tcp=new Tcp();
        this.vendedor="";
        this.data="";
        this.oskey = "";
    }
}
