import {Routes} from '@angular/router';
import { HomeComponent } from './home/home.component';
import { TcpComponent } from './tcp/tcp.component';
import { UserComponent } from './user/user.component';
import { OsComponent } from './os/os.component';
import { ProdutoComponent } from './produto/produto.component';
import { ReforcoComponent } from './reforco/reforco.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services/auth-guard.service';



export const ROUTES: Routes = [
    {path: '', component: LoginComponent},
    {path: 'home', component: HomeComponent,canActivate: [AuthGuard],},
    {path: 'TCP', component: TcpComponent,canActivate: [AuthGuard],},
    {path: 'user', component: UserComponent,canActivate: [AuthGuard],},
    {path: 'os',component: OsComponent,canActivate: [AuthGuard],},
    {path: 'produtos', component: ProdutoComponent,canActivate: [AuthGuard],},
    {path: 'reforco/:id', component: ReforcoComponent, pathMatch: 'full',canActivate: [AuthGuard],}
];
