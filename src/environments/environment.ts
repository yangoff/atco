// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBcMjcDa_7ZC7R5BjVCZ-imR5KpFo2umo8",
    authDomain: "atco-79288.firebaseapp.com",
    databaseURL: "https://atco-79288.firebaseio.com",
    projectId: "atco-79288",
    storageBucket: "atco-79288.appspot.com",
    messagingSenderId: "732544761181"
  }
};
